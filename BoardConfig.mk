# config.mk
#
# Product-specific compile-time definitions.
#
$(info "Entering Board Config")
# The generic product target doesn't have any hardware-specific pieces.
TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true

### need to modify build/core/combo/TARGET_linux-arm.mk to add arm v6 support
#TARGET_ARCH_VARIANT := armv6
#TARGET_CPU_ABI := armeabi-v6

TARGET_ARCH_VARIANT := armv5te
TARGET_CPU_ABI := armeabi
TARGET_CPU_ABI2 := armeabi
