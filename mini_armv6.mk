# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Set it up for tiny builds 
ifneq ($(filter armv6,$(TARGET_DEVICE)),)
BUILD_TINY_ANDROID := true

$(call inherit-product, build/target/product/mini.mk)

PRODUCT_NAME := mini_armv6
PRODUCT_DEVICE := armv6
PRODUCT_BRAND := Android
PRODUCT_MODEL := Mini for armv6

# default is nosdcard, S/W button enabled in resource
PRODUCT_CHARACTERISTICS := nosdcard
TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true
endif